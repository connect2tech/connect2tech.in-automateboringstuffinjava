package com.c2t.linkedin;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SecondDegreeConnection {

	WebDriver driver;
	static Logger logger = Logger.getLogger(SecondDegreeConnection.class);

	@BeforeTest
	public void before() {

		boolean ff = false;

		logger.debug("---------------------------------------------------------");
		String url = "https://www.linkedin.com/";
		/*
		 * System.setProperty("webdriver.chrome.driver",
		 * "D:/nchaurasia/Automation-Architect/connect2tech.in-AutomateBoringStuffInJava/chromedriver_win32_2.45/chromedriver.exe"
		 * );
		 */
		if (ff) {
			System.setProperty("webdriver.firefox.marionette", "geckodriver.exe");
			driver = new FirefoxDriver();
		} else {
			System.setProperty("webdriver.chrome.driver",
					"D:/nchaurasia/Automation-Architect/connect2tech.in-AutomateBoringStuffInJava/chromedriver_win32_2.45/chromedriver.exe");
			driver = new ChromeDriver();
		}
		// driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.MINUTES);
		driver.manage().timeouts().implicitlyWait(60 * 3, TimeUnit.SECONDS);
		driver.get(url);
	}

	@Test(priority = 1, enabled = true)
	public void loginToLinkedIn() {

		driver.findElement(By.id("login-email")).sendKeys("message4naresh@gmail.com");
		driver.findElement(By.id("login-password")).sendKeys("Modified12#$");
		driver.findElement(By.id("login-submit")).click();
	}

	@Test(priority = 2, enabled = true)
	public void goToPeopleURL() throws Exception {
		driver.navigate().to("https://www.linkedin.com/search/results/people/");
	}

	@Test(priority = 3, enabled = true)
	public void clickOnConnections() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 60 * 2);
		wait.until(ExpectedConditions.elementToBeClickable(By.className("search-s-facet__form")));

		WebElement we1 = driver.findElement(By.className("search-s-facet__form"));
		we1.click();

		we1.findElements(By.className("search-s-facet-value")).get(1).click();

		we1.findElement(By.className("facet-collection-list__apply-button")).click();

	}

	@Test(priority = 4, enabled = true)
	public void getSearchResultRows() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions
				.numberOfElementsToBe(By.xpath("//li[contains(@class,'earch-result__occluded-item')]"), 10));

		List<WebElement> list2 = driver.findElements(By.xpath("//li[contains(@class,'earch-result__occluded-item')]"));
		logger.debug("list2==>" + list2);
		logger.debug("list.size()==>" + list2.size());

	}

	@Test(priority = 5, enabled = false)
	public void navigateNext_10_Pages() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 60 * 2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("page-list")));

		// List<WebElement> links =
		// driver.findElements(By.xpath("//*[contains(@id,'ember')]/li[1]/ol/li"));
		WebElement links = driver.findElement(By.className("page-list"));
		// page-list
		logger.debug("links==>" + links);

		List<WebElement> lis = links.findElements(By.tagName("li"));
		logger.debug("lis==>" + lis);

		for (int i = 0; i < lis.size(); i++) {
			logger.debug("lis.get(i).getText()==>" + lis.get(i).getText());
			lis.get(i).click();
		}
	}

	@Test(priority = 5, enabled = true)
	public void scrollDownToBottom() throws Exception {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	@Test(priority = 6, enabled = true)
	public void navigateNext() throws Exception {

		// WebDriverWait wait = new WebDriverWait(driver, 3 * 60);
		// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".results-paginator"))));

		// List<WebElement> links =
		// driver.findElements(By.xpath("//*[contains(@id,'ember')]/li[1]/ol/li"));
		WebElement next = driver.findElement(By.xpath("//*[starts-with(@id,'ember')]/li[2]/button"));
		// WebElement next =
		// driver.findElement(By.cssSelector(".results-paginator"));

		// page-list
		logger.debug("next==>" + next);
		logger.debug("next.getText()==>" + next.getText());

	}

	// *[contains(@id,'ember')]/li[1]/ol/li

}
