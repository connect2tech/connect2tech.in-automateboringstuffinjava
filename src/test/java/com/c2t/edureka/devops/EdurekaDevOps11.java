package com.c2t.edureka.devops;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class EdurekaDevOps11 {

	WebDriver driver;
	Pattern module2;
	Pattern module8;
	Pattern module11;
	Pattern rightArrow;
	Pattern clickNext;
	Pattern clickMax;
	Screen screen;

	@BeforeTest
	public void before() {

		module2 = new Pattern("D:/nchaurasia/Automation-Architect/img/Module2.PNG");
		module8 = new Pattern("D:/nchaurasia/Automation-Architect/img/Module8.PNG");
		module11 = new Pattern("D:/nchaurasia/Automation-Architect/img/Module11-2");
		rightArrow = new Pattern("D:/nchaurasia/Automation-Architect/img/Right-Arrow.PNG");
		clickNext = new Pattern("D:/nchaurasia/Automation-Architect/img/ClickNext.PNG");
		clickMax = new Pattern("D:/nchaurasia/Automation-Architect/img/ClickMaximizePpt.PNG");
		screen = new Screen();

		String url = "https://www.edureka.co/";
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-AutomateBoringStuffInJava/chromedriver_win32_2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(2, TimeUnit.MINUTES);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(url);
	}



	@Test(priority = 1, enabled = true)
	public void loginToEdureka() throws FindFailed, Exception {

		driver.findElement(By.xpath("//*[@id='header-II']/section/nav/div/a[2]")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("si_popup_email")).sendKeys("message4naresh@gmail.com");
		driver.findElement(By.xpath("//*[@id='si_popup_passwd']")).sendKeys("NewPassword12#$");
		driver.findElement(By.xpath("//*[@id='new_sign_up_mode']/div/div/div[2]/div[3]/form/button")).click();
	}

	@Test(priority = 2, enabled = true)
	public void clickGoToDevOps() {

		driver.findElement(By
				.xpath("//*[@id='footauto']/app-root/app-mycourse-main/section/section[2]/div/div/div/div[4]/div[4]/div[2]/a"))
				.click();

	}

	@Test(priority = 3, enabled = true)
	public void clickCourseContent() {

		driver.findElement(By
				.xpath("//*[@id='footauto']/app-root/app-curriculum-main/section[3]/div/div/app-curriculum-leftnav/div/ul/li[4]/a"))
				.click();

	}

	@Test(priority = 4, enabled = true)
	public void click_Module_10() throws FindFailed,Exception {
		Thread.sleep(5000);
		System.out.println("Click Right Arrow...");
		//screen.wait(rightArrow, 10);
		//screen.click(rightArrow);
		//Thread.sleep(5000);
		screen.wait(module11, 10);
		screen.click(module11);
	}

	@Test(priority = 5, enabled = true)
	public void click_Presentation() throws FindFailed {
		System.out.println("click_Presentation...");
		driver.findElement(By.xpath("//*[@id='mat-tab-content-0-12']/div/div/div/a[2]/div")).click();
		

	}

	@Test(priority = 6, enabled = true)
	public void click_Next() throws FindFailed, InterruptedException, Exception {

		Thread.sleep(60000);
		screen.wait(clickMax, 5);
		screen.click(clickMax);
		Thread.sleep(5000);
		for (int i = 1; i <= 53; i++) {
			sreenShot(i);
			screen.wait(clickNext, 5);
			screen.click(clickNext);
			Thread.sleep(5000);

		}
	}

	public void sreenShot(int screenNo) throws Exception {
		TakesScreenshot sc = (TakesScreenshot) driver;

		File screenShot = sc.getScreenshotAs((OutputType.FILE));

		String screenName = "D:/nchaurasia/Automation-Architect/connect2tech.in-AutomateBoringStuffInJava/DevOps-module11/"
				+ "DevOps" + screenNo + ".png";

		FileUtils.copyFile(screenShot, new File(screenName));
	}

}
